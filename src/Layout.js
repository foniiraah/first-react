import React,  { useState } from "react";
import { 
  BrowserRouter,
  Link
 } from "react-router-dom";
import {
    ProSidebar,
    Menu,
    MenuItem,
    SidebarHeader,
    SidebarFooter,
    SidebarContent
  } from "react-pro-sidebar";
  import { FaList, FaRegHeart } from "react-icons/fa";
  import {
    FiHome,
    FiLogOut,
    FiArrowLeftCircle,
    FiArrowRightCircle
  } from "react-icons/fi";
  import { BiCog } from "react-icons/bi";

const Layout = () => {
//   create initial menuCollapse state using useState hook
  const [menuCollapse, setMenuCollapse] = useState(false);
  //create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    //condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };

  return (
    <div id="layout">
        <ProSidebar collapsed={menuCollapse}>
          <SidebarHeader>
             <div className="logotext">
               {/* small and big change using menucollapse state */}
               <p>{menuCollapse ? "Toko Hape" : "Toko Hape"}</p>
             </div>
             <div className="closemenu" onClick={menuIconClick}>
               {/* changing menu collapse icon on click */}
               {menuCollapse ? <FiArrowRightCircle /> : <FiArrowLeftCircle />}
             </div>
           </SidebarHeader>
           <SidebarContent>
             <Menu iconShape="square">
               <MenuItem active={true} icon={<FiHome />}><span><Link to="/">Home</Link></span></MenuItem>
               <MenuItem icon={<FaList />}><span><Link to="/menuphone">List Produk</Link></span></MenuItem>
               <MenuItem icon={<FaRegHeart />}>Favourite</MenuItem>
               <MenuItem icon={<BiCog />}>Settings</MenuItem>
             </Menu>
           </SidebarContent>
           <SidebarFooter>
             <Menu iconShape="square">
               <MenuItem icon={<FiLogOut />}>Logout</MenuItem>
             </Menu>
           </SidebarFooter>
         </ProSidebar>
    </div>
  );
};
export default Layout;