import { useRoutes  } from "react-router-dom";
import Layout from "./Layout";
import MenuPhone from "./MenuPhone";
import Title from "./Title";

export default function Router() {
    let element = useRoutes ([
        {
            path: "/menuphone", 
            element: <MenuPhone />
        },
        {
            path: "/",
            element: <Title/>
        }
    ])

    return element;
}