import logo from './logo.svg';
import "./Layout.css";
import Layout from './Layout';
// import Title from './Title';
// import MenuPhone from './MenuPhone';
import Router from './IndexRoutes';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <div className='App'>
        <BrowserRouter>
          <Router/>
          <Layout/>
          {/* <Title /> */}
        </BrowserRouter>
        {/* <MenuPhone /> */}
    </div>
  );
}

export default App;
