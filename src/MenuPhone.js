import React, { useRef, useState } from "react";
import servicePhone from "./service";
import Table from 'react-bootstrap/Table';
import Modal from "react-bootstrap/Modal";
import { Container, Card } from "@material-ui/core";
import { Button, Form, FloatingLabel } from "react-bootstrap";

function MenuPhone() {

    const[getResult, setGetResult] = useState([]);
    const[putResult, setPutResult] = useState([]);

    const ids = useRef(null);
    const namephone = useRef(null);
    const price = useRef(null);
    const discount = useRef(null);
    const [postResult, setPostResult] = useState([]);
    const [show, setShow] = useState(false);
    const [showAdd, setShowAdd] = useState(false);

    const handleClose = () =>  setShow(false);
    const handleCloseAdd = () => setShowAdd(false);
    const handleOpenAdd = () => setShowAdd(true);
    
    const getAllData = async () => {
        try {
            const res = await servicePhone.get("/dataphone") .then((result) => result.data);
            console.log(res);
                setGetResult(res);
            } catch (err) {
                setGetResult((err.response || err));
            }
        }
    React.useEffect(() => {
        getAllData();
        postData();
    }, [])

    async function postData() {
        const posting = {
            namephone: namephone.current.value,
            price: price.current.value,
            discount: discount.current.value,
        };
        console.log(posting);
        try {
            const res = await servicePhone.post("/dataphone", posting) .then((result) => result.data);
            console.log(res);
            setPostResult(res);
            window.location.reload();
            setPostResult(res);
        } catch (err) {
            setPostResult((err.response || err));
        }
    }

    const handleOpen = () => setShow(true);
    async function updateData() {
        const id = ids.current.value;
        if (id) {
            const putData = {
                namephone: namephone.current.value,
                price: price.current.value,
                discount: price.current.value
            };
        console.log(this.id);
        try {
            const res = await servicePhone.put("/dataphone/${id}", putData) .then((result) => result.data);
            console.log(res);
            setPutResult(res);
            window.location.reload();
            setPutResult(res);
        } catch (err) {
            setPutResult((err.response || err));
        }
    }
    }

    return (
        <>
        <div className="menu-phone">
            <Container fluid="md">
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header>
                        <h3> Ubah Data </h3>
                    </Modal.Header>
                    <Modal.Body>
                        <FloatingLabel controlId="floatingInput" label="Nama" className="mb-3">
                            <Form.Control type="text" placeholder="Nama" value={namephone} />
                        </FloatingLabel>
                        <FloatingLabel controlId="floatingInput" label="Harga" className="mb-3">
                            <Form.Control type="number" placeholder="Harga" value={price} />
                        </FloatingLabel>
                        <FloatingLabel controlId="floatingInput" label="Discount" className="mb-3">
                            <Form.Control type="text" placeholder="Discount" value={discount} />
                        </FloatingLabel>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-sm btn-primary" onClick={updateData}>Ubah</button>
                    </Modal.Footer>
                </Modal>
                <Button className="btn" onClick={handleOpenAdd}> Tambah </Button>
                    <Modal show={showAdd} onHide={handleCloseAdd}>
                        <Modal.Header>
                            <h3> Tambah Data </h3>
                        </Modal.Header>
                        <Modal.Body>
                            <FloatingLabel controlId="floatingInput" label="Nama" className="mb-3">
                                <Form.Control type="text" placeholder="Nama" ref={namephone} />
                            </FloatingLabel>
                            <FloatingLabel controlId="floatingInput" label="Harga" className="mb-3">
                                <Form.Control type="number" placeholder="Harga" ref={price} />
                            </FloatingLabel>
                            <FloatingLabel controlId="floatingInput" label="Discount" className="mb-3">
                                <Form.Control type="text" placeholder="Discount" ref={discount} />
                            </FloatingLabel>
                        </Modal.Body>
                        <Modal.Footer>
                        <button className="btn btn-sm btn-primary" onClick={postData}>Posting</button>
                        </Modal.Footer>
                    </Modal>
                <br/>
                <div className="Table">
                    <Table striped size="lg">
                        <thead>
                            <tr>
                                <th>Nama Handphone</th>
                                <th>Price</th>
                                <th>discount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        {getResult.map(data => (
                            <tr key={data.id}>
                                <td>{data.namephone}</td>
                                <td>{data.price}</td>
                                <td>{data.discount}</td>
                                <td><button onClick={handleOpen}>Edit</button></td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </div>
            </Container>
        </div>
        </>
    );
}
export default MenuPhone;